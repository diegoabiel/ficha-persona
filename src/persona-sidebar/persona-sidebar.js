import { LitElement, html } from 'lit-element';  
	class PersonaSidebar extends LitElement {
	static get properties() {
		return {			
		};
	}

	constructor() {
		super();			
	}

	render() {
		return html`		
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">	
			<aside>
					<div class="mt-5">
						<button class="w-100 btn bg-success" style="font-size: 50px" @click="${this.newPerson}"><strong>+</strong></button>
					<div>
			</aside>
		`;
	}

	newPerson(e) {
		console.log("newPerson en persona-sidebar");
		console.log("Se va a crear una nueva persona");
  
		this.dispatchEvent(new CustomEvent("new-person", {}));
	}
}
customElements.define('persona-sidebar', PersonaSidebar)